FROM golang:1.17-bullseye AS build

ARG GOPRIVATE=gitlab.com/assembly4

RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
RUN git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN --mount=type=ssh go mod download
COPY . .
RUN go build -o /server/bin ./server


FROM gcr.io/distroless/base-debian11

COPY --from=build /server/bin /server

ENTRYPOINT ["/server"]
