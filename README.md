# geneva

Register, login, and JWT verification service for assembly project

## API Reference

#### Check health of a service and underlying DB
```bash
  curl --location --request GET 'http://geneva.localhost/api/v1/health'
```

#### Register new user

```bash
curl --location --request POST 'http://geneva.localhost/api/v1/register' \
--form 'email="user@domain.com"' \
--form 'password="superpass"'
```

#### Login user

```bash
curl --location --request POST 'http://geneva.localhost/api/v1/login' \
--form 'email="user@domain.com"' \
--form 'password="superpass"'
```

#### Get users

```bash
curl --location --request GET 'http://geneva.localhost/api/v1/users?queryString=<queryString>' --header 'Authorization: Bearer <token>'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `queryString`  | `string` | **Required**. User search pattern |

#### Get friendships

```bash
curl --location --request GET 'http://geneva.localhost/api/v1/friendships?status=<status>' --header 'Authorization: Bearer <token>'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `status`  | `string` | **Required**. oneof [AcceptedRequested/Declined] |

#### Create/update friendship

```bash
curl --location --request PUT 'http://geneva.localhost/api/v1/friendships' --header 'Authorization: Bearer <token>' --form 'addressee="<addressee>"' --form 'status="<status>"'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `addressee`   | `string` | **Required**. Addressee email |
| `status`  | `string` | **Required**. oneof [AcceptedRequested/Declined] which either creates or updates|

#### Delete friendship

```bash
curl --location --request DELETE 'http://geneva.localhost/api/v1/friendships?addressee=<addressee>' --header 'Authorization: Bearer <token>'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `addressee`   | `string` | **Required**. Addressee email |

#### Get devices

```bash
curl --location --request GET 'http://geneva.localhost/api/v1/devices?addressee=<addressee>' --header 'Authorization: Bearer <token>'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `addressee`  | `string` |  Email of a user you want to get devices of, `friend's email`=friend's devices, `self email`=own devices, `blank`=own+friends' devices|

#### Create/update device

```bash
curl --location --request PUT 'http://geneva.localhost/api/v1/devices' --header 'Authorization: Bearer <token>' --form 'name="<name>"' --form 'uuid="<uuid>"'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `name`   | `string` | **Required**. Name for the device |
| `uuid`  | `string` | **Required**. Uuid of the device you want to create/update|

#### Delete device

```bash
curl --location --request DELETE 'http://geneva.localhost/api/v1/devices?uuid=<uuid>' --header 'Authorization: Bearer <token>'
```
| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `token`   | `string` | **Required**. Your JWT token |
| `uuid`   | `string` | **Required**. Uuid of the device to delete |

## Deployment

To deploy this project locally run

```bash
helm uninstall $(basename "$PWD") -n $(basename "$PWD"); docker build --ssh default=~/.ssh/id_rsa -t registry.gitlab.com/assembly4/$(basename "$PWD"):latest .; helm upgrade $(basename "$PWD") ./chart/ -i --history-max 0 -n $(basename "$PWD");
```

## Environment Variables

To run this project, you will need to deploy terraform which provisions needed secrets and configmaps - [link to repo](https://gitlab.com/assembly4/terraform)
