package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func deleteDevice(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var (
		id     uuid
		exists bool
	)

	if err := c.BindQuery(&id); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	user := c.GetString("user")

	if err := db.QueryRow(ctx, `SELECT EXISTS(SELECT 1 FROM devices WHERE uuid=$1)`, id.Data).Scan(&exists); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	if !exists {
		log.Printf("device %s doesn't exist", id.Data)
		c.JSON(400, gin.H{"message": "device doesn't exist"})
		return
	}

	_, err := db.Exec(ctx, `DELETE FROM devices WHERE 
	owner=(SELECT id FROM users WHERE email=$1) AND uuid=$2`, user, id.Data)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	log.Printf("device %s deleted", id.Data)
	c.JSON(200, gin.H{"message": "deleted"})
}
