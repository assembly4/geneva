package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4"
	"golang.org/x/crypto/bcrypt"
)

type UserInfo struct {
	Email string
}

type CustomClaims struct {
	*jwt.StandardClaims
	*UserInfo
}

func login(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var (
		user     User
		userInDb User
	)

	if err := c.BindJSON(&user); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	err := db.QueryRow(
		ctx,
		"SELECT email,password FROM users WHERE email=$1",
		user.Email).Scan(&userInDb.Email, &userInDb.Password)
	if err == pgx.ErrNoRows {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid credentials"})
		return
	}
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(userInDb.Password), []byte(user.Password)); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid credentials"})
		return
	}

	token, err := createToken(user.Email)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "error creating jwt"})
		return
	}

	log.Printf("%s logged in successfully", user.Email)
	c.JSON(200, gin.H{"message": "ok", "access_token": token})
}
