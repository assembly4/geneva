package main

import (
	"time"

	"github.com/golang-jwt/jwt"
)

func createToken(email string) (string, error) {
	t := jwt.New(jwt.SigningMethodRS512)

	t.Claims = &CustomClaims{
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(tokenLifeTime)).Unix(),
		},
		&UserInfo{
			Email: email,
		},
	}

	return t.SignedString(privateKey)
}
