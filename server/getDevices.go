package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func getDevices(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var (
		devices     []device
		o           ownerEmail
		exists      bool
		friendShips []friendShip
	)

	if err := c.BindQuery(&o); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	user := c.GetString("user")
	f := &friendshipStatus{
		Status: "Accepted",
	}

	// add user to friendship slice
	friendShips = append(friendShips, friendShip{
		Addressee: user,
	})

	// only devices of one user
	if o.Email != "" {
		if o.Email == user {
			// select only own devices
			if err := listDevices(&friendShips, &devices); err != nil {
				log.Println(err)
				c.JSON(500, gin.H{"message": "database error"})
				return
			}
			log.Printf("returning own devices for %s", user)
			c.JSON(200, gin.H{"message": "ok", "devices": &devices})
			return
		}

		//check if addressee is a friend with the user
		if err := db.QueryRow(ctx, `SELECT EXISTS(
			SELECT 1 FROM friendship f
				INNER JOIN users u1 ON (f.requester_id = u1.id)
				INNER JOIN users u2 ON (f.addressee_id = u2.id)
				INNER JOIN status s ON (f.status_code = s.status_code)
			WHERE (
				(u1.email=$1 AND u2.email=$2) OR (u1.email=$2 AND u2.email=$1)
			) AND s.name=$3)`,
			user, o.Email, f.Status).Scan(&exists); err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"message": "database error"})
			return
		}
		if !exists {
			log.Printf("%s is not a friend", o.Email)
			c.JSON(403, gin.H{"message": "not a friend"})
			return
		}

		// clear slice and then only add addressee
		friendShips = nil
		friendShips = append(friendShips, friendShip{
			Addressee: o.Email,
		})
		if err := listDevices(&friendShips, &devices); err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"message": "database error"})
			return
		}

		log.Printf("returning %s's devices", o.Email)
		c.JSON(200, gin.H{"message": "ok", "devices": &devices})
		return
	}

	// get all friends
	if err := listFriendships(user, &friendShips, f); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "error geting friends"})
		return
	}

	// devices from all friends and user's (all devices)
	if err := listDevices(&friendShips, &devices); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	log.Printf("returning all devices for %s", user)
	c.JSON(200, gin.H{"message": "ok", "devices": &devices})
}

func listDevices(friends *[]friendShip, devices *[]device) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var (
		device   device
		friendos []string
	)

	for _, p := range *friends {
		friendos = append(friendos, p.Addressee)
	}

	// select all devices that user has access to
	rows, _ := db.Query(ctx, `SELECT d.name, u.email, d.uuid FROM devices d 
	INNER JOIN users u ON (d.owner = u.id)
	WHERE u.email = ANY ($1)`, friendos)
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&device.Name, &device.Owner, &device.Uuid); err != nil {
			return err
		}
		*devices = append(*devices, device)
	}

	return rows.Err()
}
