package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func putDevice(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var (
		d       device
		exists  bool
		updated bool
	)

	if err := c.Bind(&d); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	user := c.GetString("user")

	if err := db.QueryRow(ctx, `SELECT EXISTS(SELECT 1 FROM devices WHERE uuid=$1)`, d.Uuid).Scan(&exists); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	if !exists {
		_, err := db.Exec(ctx, `INSERT INTO devices(owner,name,uuid) VALUES (
			(SELECT id FROM users WHERE email=$1),$2,$3)`, user, d.Name, d.Uuid)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"message": "database error"})
			return
		}

		log.Printf("device %s created", d.Uuid)
		c.JSON(201, gin.H{"message": "created"})
		return
	}

	if err := db.QueryRow(ctx, `WITH child AS (
		UPDATE devices SET name=$1 
		WHERE 
		uuid=$2 AND owner=(SELECT id FROM users WHERE email=$3)
		RETURNING 1
	) SELECT EXISTS(SELECT 1 FROM child)`, d.Name, d.Uuid, user).Scan(&updated); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}
	if !updated {
		log.Printf("device %s not updated", d.Uuid)
		c.JSON(400, gin.H{"message": "not updated"})
		return
	}

	log.Printf("device %s updated", d.Uuid)
	c.JSON(200, gin.H{"message": "updated"})
}
