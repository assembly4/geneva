package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func getUsers(c *gin.Context) {
	var (
		users []string
		qs    queryString
	)

	if err := c.BindQuery(&qs); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	r := c.GetString("user")
	if qs.Text == "" {
		qs.Text = "%"
	}

	if err := listUsers(r, qs.Text, &users); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	log.Printf("returning users matching %s", qs.Text)
	c.JSON(200, gin.H{
		"message": "ok",
		"users":   users,
	})
}

func listUsers(requester string, queryString string, users *[]string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var user string

	queryString = "%" + queryString + "%"

	rows, _ := db.Query(ctx, "SELECT email FROM users WHERE email LIKE $1 AND email !=$2", queryString, requester)
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&user); err != nil {
			return err
		}
		*users = append(*users, user)
	}

	return rows.Err()
}
