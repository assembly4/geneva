package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/assembly4/bern"
)

type Token struct {
	Token string `form:"token" json:"token" binding:"required,jwt"`
}

func verify(c *gin.Context) {
	var token Token

	if err := c.BindJSON(&token); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	if _, err := bern.VerifyToken(&token.Token, publicKey); err != nil {
		c.JSON(400, gin.H{"message": err.Error()})
		return
	}

	log.Printf("token valid")
	c.JSON(200, gin.H{"message": "ok"})
}
