package main

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
)

func healthCheck(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(3*time.Second))
	defer cancel()

	if err := db.Ping(ctx); err != nil {
		c.JSON(503, gin.H{
			"message": "unhealthy",
			"db":      err,
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "healthy",
		"db":      "ok",
	})
}
