package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func putFriendShip(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var (
		f       friendShip
		exists  bool
		updated bool
	)

	if err := c.Bind(&f); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	user := c.GetString("user")

	if user == f.Addressee {
		c.JSON(400, gin.H{"message": "cannot be friends with yourself"})
		return
	}

	// check if friendship already exists from any side
	if err := db.QueryRow(ctx, `SELECT EXISTS(
		SELECT 1 FROM friendship f
			INNER JOIN users u1 ON (f.requester_id = u1.id) 
			INNER JOIN users u2 ON (f.addressee_id = u2.id) 
			WHERE (u1.email=$1 AND u2.email=$2) OR (u1.email=$2 AND u2.email=$1))`,
		user, f.Addressee).Scan(&exists); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	switch f.Status {
	case "Requested":
		if exists {
			log.Printf("friendship %s - %s already exists", user, f.Addressee)
			c.JSON(400, gin.H{"message": "friendship already exists"})
			return
		}

		// create friendship with user as the requester
		_, err := db.Exec(ctx, `INSERT INTO friendship(requester_id, addressee_id, status_code) 
		VALUES (
			(SELECT id FROM users WHERE email=$1),
			(SELECT id FROM users WHERE email=$2),
			(SELECT status_code FROM status WHERE name=$3))`, user, f.Addressee, f.Status)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"message": "database error"})
			return
		}

		log.Printf("friendship %s - %s created", user, f.Addressee)
		c.JSON(201, gin.H{"message": "created"})
		return
	default:
		if !exists {
			log.Printf("friendship %s - %s doesn't exist", user, f.Addressee)
			c.JSON(400, gin.H{"message": "friendship doesn't exist"})
			return
		}
		// user can update to Accepted/Declined status only if he/she is addressee
		if err := db.QueryRow(ctx, `WITH child AS (
			UPDATE friendship 
			SET status_code=(SELECT status_code FROM status where name=$1) 
			WHERE requester_id=(SELECT id FROM users WHERE email=$2) AND 
			addressee_id=(SELECT id FROM users WHERE email=$3)
			RETURNING 1
		) SELECT EXISTS(SELECT 1 FROM child)`, f.Status, f.Addressee, user).Scan(&updated); err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"message": "database error"})
			return
		}
		if !updated {
			c.JSON(400, gin.H{"message": "not updated"})
			return
		}
	}

	log.Printf("friendship %s - %s updated", user, f.Addressee)
	c.JSON(200, gin.H{"message": "updated"})
}
