package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func getFriendships(c *gin.Context) {
	var (
		friendShips []friendShip
		r           userEmail
		fst         friendshipStatus
	)

	if err := c.BindQuery(&fst); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	r.Email = c.GetString("user")

	if err := listFriendships(r.Email, &friendShips, &fst); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	log.Printf("returning %s friendsips for %s", fst.Status, r.Email)
	c.JSON(200, gin.H{
		"message": "ok",
		"users":   friendShips,
	})
}

func listFriendships(requesterEmail string, friendShips *[]friendShip, fst *friendshipStatus) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var fs friendShip

	// get all friendships (both as requester and addressee)
	rows, _ := db.Query(ctx,
		`SELECT u1.email AS requester, u2.email AS addressee, s.name AS status
			FROM friendship f
				INNER JOIN status s ON (f.status_code = s.status_code)
				INNER JOIN users u1 ON (f.requester_id = u1.id)
				INNER JOIN users u2 ON (f.addressee_id = u2.id)
			WHERE s.name=$1 AND (u1.email=$2 OR u2.email=$2)`,
		fst.Status, requesterEmail)
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&fs.Requester, &fs.Addressee, &fs.Status); err != nil {
			return err
		}
		*friendShips = append(*friendShips, fs)
	}

	return rows.Err()
}
