package main

type User struct {
	Email    string `json:"email" db:"email" binding:"required,email,lowercase"`
	Password string `json:"password" db:"password" binding:"required"`
}

type userEmail struct {
	Email string `form:"addressee" json:"addressee" binding:"required,email,lowercase"`
}

type ownerEmail struct {
	Email string `form:"addressee" json:"addressee" binding:"omitempty,email,lowercase"`
}

type queryString struct {
	Text string `form:"queryString" json:"queryString" binding:"omitempty,lowercase"`
}

type friendShip struct {
	Status    string `json:"status" binding:"oneof=Accepted Declined Requested"`
	Requester string `json:"requester"`
	Addressee string `json:"addressee" binding:"required,email,lowercase"`
}

type friendshipStatus struct {
	Status string `form:"status" json:"status" db:"name" binding:"oneof=Accepted Declined Requested"`
}

type device struct {
	Name  string `json:"name" binding:"required,lte=25"`
	Owner string `json:"owner"`
	Uuid  string `json:"uuid" binding:"required,uuid4"`
}

type uuid struct {
	Data string `form:"uuid" json:"uuid" binding:"required,uuid4"`
}
