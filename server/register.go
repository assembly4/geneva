package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func register(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(15*time.Second))
	defer cancel()

	var (
		user   User
		exists bool
	)

	if err := c.BindJSON(&user); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}
	if err := db.QueryRow(ctx, "SELECT EXISTS(SELECT 1 FROM users WHERE email=$1)", user.Email).Scan(&exists); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	if exists {
		log.Printf("%s already exists", user.Email)
		c.JSON(400, gin.H{"message": "user already exists"})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "hashing error"})
		return
	}

	_, err = db.Exec(
		ctx,
		"INSERT INTO users(email, password) VALUES ($1, $2)",
		user.Email,
		string(hashedPassword),
	)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	token, err := createToken(user.Email)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "error creating jwt"})
		return
	}

	log.Printf("%s registered successfully", user.Email)
	c.JSON(201, gin.H{"message": "ok", "access_token": token})
}
