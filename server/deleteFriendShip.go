package main

import (
	"context"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func deleteFriendShip(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()

	var (
		a       userEmail
		deleted bool
	)

	if err := c.BindQuery(&a); err != nil {
		log.Println(err)
		c.JSON(400, gin.H{"message": "invalid data"})
		return
	}

	user := c.GetString("user")

	if user == a.Email {
		log.Printf("friendship %s - %s do not specify friendship with yourself", user, a.Email)
		c.JSON(400, gin.H{"message": "do not specify friendship with yourself"})
	}

	if err := db.QueryRow(ctx, `WITH child as (
		DELETE FROM friendship f USING users u1, users u2 WHERE 
		((f.requester_id=u1.id AND u1.email=$1) AND (f.addressee_id=u2.id AND u2.email=$2))
		OR 
		((f.requester_id=u1.id AND u1.email=$2) AND (f.addressee_id=u2.id AND u2.email=$1)) 
		RETURNING 1
	) SELECT EXISTS(SELECT 1 FROM child)`, user, a.Email).Scan(&deleted); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{"message": "database error"})
		return
	}

	if !deleted {
		log.Printf("friendship %s - %s doesn't exist", user, a.Email)
		c.JSON(400, gin.H{"message": "friendship doesn't exist"})
		return
	}

	log.Printf("friendship %s - %s deleted", user, a.Email)
	c.JSON(200, gin.H{"message": "deleted"})
}
