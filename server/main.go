package main

import (
	"context"
	"crypto/rsa"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/assembly4/bern"
)

var (
	privateKey    *rsa.PrivateKey
	publicKey     *rsa.PublicKey
	db            *pgxpool.Pool
	tokenLifeTime int
)

func main() {
	defer db.Close()

	requiredEnv := []string{"PRIVATE_KEY", "PUBLIC_KEY", "TOKEN_LIFETIME", "PGHOST", "PGDATABASE", "PGPASSWORD"}
	bern.CheckEnvVars(requiredEnv)

	config := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s application_name=%s pool_max_conns=%d pool_min_conns=%d",
		os.Getenv("PGHOST"),
		os.Getenv("PGUSER"),
		os.Getenv("PGPASSWORD"),
		os.Getenv("PGDATABASE"),
		"geneva",
		5,
		1,
	)

	poolConfig, err := pgxpool.ParseConfig(config)
	if err != nil {
		log.Fatalf("unable to parse DB config: %v", err)
	}
	db, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		log.Fatalf("unable to connect to database: %v", err)
	}

	tokenLifeTime, err = strconv.Atoi(os.Getenv("TOKEN_LIFETIME"))
	if err != nil {
		log.Fatalf("unable to convert TOKEN_LIFETIME variable: %v", err)
	}

	privateKey, err = bern.LoadPrivateKey(os.Getenv("PRIVATE_KEY"))
	if err != nil {
		log.Fatalf("unable to load private key: %v", err)
	}
	publicKey, err = bern.LoadPublicKey(os.Getenv("PUBLIC_KEY"))
	if err != nil {
		log.Fatalf("unable to load public key: %v", err)
	}

	r := gin.Default()
	cors.Default()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:    []string{"Authorization", "Origin", "Content-Length", "Content-Type"},
	}))

	v1 := r.Group("/api/v1")
	{
		v1.GET("/health", healthCheck)

		v1.POST("/verify", verify)
		v1.POST("/login", login)
		v1.POST("/register", register)

		auth := v1.Group("").Use(bern.JWTAuthMiddleware(publicKey))
		{
			auth.GET("/users", getUsers)

			auth.GET("/friendships", getFriendships)
			auth.PUT("/friendships", putFriendShip)
			auth.DELETE("/friendships", deleteFriendShip)

			auth.GET("/devices", getDevices)
			auth.PUT("/devices", putDevice)
			auth.DELETE("/devices", deleteDevice)
		}
	}

	r.Run()
}
